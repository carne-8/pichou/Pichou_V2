FROM node

WORKDIR /app
COPY yarn.lock .
COPY package.json .

RUN yarn
COPY build ./src/
COPY package.json ./


EXPOSE 3874

CMD [ "node", "src/App.js" ]