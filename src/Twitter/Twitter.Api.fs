module Twitter.API

open System

open Fable.Core
open Thoth.Fetch

let token = "Bearer " + getEnv "TWITTER_TOKEN"
let sncfTweetUrl = getEnv "TWITTER_DISRUPTION_URL"

let headers =
    [ Fetch.Types.Authorization token ]

let getLastSncfTweets () : JS.Promise<Result<Types.API.Twitter.ApiResponse, FetchError>> =
    Fetch.tryGet(sncfTweetUrl, headers = headers)