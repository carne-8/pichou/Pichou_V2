module Sncf

open System
open Types.Discord
open Thoth.Fetch
open Fable.Core
open YoLo
open Fetch.Types

let formatTweet (tweet: Types.API.Twitter.Tweet) =
    let tweet =
        tweet.text
            |> String.Replace "Trains" "Word to not delete"
            |> String.Replace "Train" "🚄"
            |> String.Replace "Word to not delete" "Trains"
            |> String.Replace "Alarm clock" "⏰"
            |> String.Replace "Wastebasket" "🗑️"
            |> String.Replace "⌚" "⏱️"

            |> String.Split [| '\n' |]
            |> Array.RemoveLast
            |> String.concat "\n"

    let date =
        tweet
        |> String.Split [| '\n' |]
        |> Array.head
        |> String.Remove -2

    let trainInfo =
        tweet
        |> String.Split [| '\n' |]
        |> Array.tail
        |> Array.map (fun str ->
            let splitLine = str.Split [| ':' |]
            sprintf "%s:**%s**" splitLine.[0] splitLine.[1]
        )
        |> String.concat "\n"

    (date,
     trainInfo)


let tweetToEmbed (discord: IDiscord) (client: IClient) date trainInfo =
    discord.CreateEmbed()
    |> Embed.setColor "00ffdf"
    |> Embed.setThumbnail (getEnv "SNCF_ICON")
    |> Embed.setAuthor "Pichou" (client.user.displayAvatarURL()) (getEnv "INVITATION_URL")
    |> Embed.addField (date + " :") trainInfo false
    |> Embed.setFooter (date) (Some (client.user.displayAvatarURL()))


let postSncfLate discord client (msg: IDiscordMsg) =
    promise {
        let! tweetsResult = Twitter.API.getLastSncfTweets()
        match tweetsResult with
        | Ok tweets ->
            let (date, trainInfo) =
                tweets.data.[2]
                |> formatTweet

            let embed = tweetToEmbed discord client date trainInfo
            msg.channel.send embed |> ignore
        | Error error ->
            discord.CreateEmbed()
                |> Embed.setColor "#ff0000"
                |> Embed.addField "Oups !" "Il y a eu une erreur." true
                |> Embed.addField "Erreur: " (JS.JSON.stringify error) true
                |> msg.channel.send
                |> ignore
    } |> Promise.start

// tweetLines
// (fun disruption ->
//     match disruption.severity.effect with
//     | Sncf.Types.Effect.SignificantDelays ->
//         disruption.impacted_objects
//         |> Array.iter (fun impactedObject ->
//             impactedObject.impacted_stops.Value
//             |> Array.iter (fun impactedStop ->
//                 match (impactedStop.base_arrival_time, impactedStop.amended_arrival_time) with
//                 | Some baseArrivalTime, Some amendedArrivalTime ->
//                     let baseArrivalHours = baseArrivalTime.[..1] |> int
//                     let baseArrivalMinutes = baseArrivalTime.[2..3] |> int

//                     let amendedArrivalHours = amendedArrivalTime.[..1] |> int
//                     let amendedArrivalMinutes = amendedArrivalTime.[2..3] |> int

//                     let hoursLate = amendedArrivalHours - baseArrivalHours
//                     let minutesLate = amendedArrivalMinutes - baseArrivalMinutes

//                     late <- TimeSpan(hoursLate, minutesLate, 0) |> late.Add
//                 | _ -> ignore ignore
//             )
//         )
//     | Sncf.Types.Effect.NoService ->
//         canceled <- disruption.impacted_objects.Length
//     | _ -> ()
// )

// printfn "%A" (momentJs?duration(late.TotalMinutes, "minutes")?format("y [years], w [weeks], d [days], h [hours], m [minutes]"))
// printfn "%A" canceled