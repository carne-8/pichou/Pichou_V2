module Money

open System
open Fable.Core
open Types.Discord
open Thoth.Fetch

type Money =
    {
        _id: string
        discordID: string
        money: int
    }

type FilterMoneysError =
    | MoneyNotFound
    | MultipleMoney of Money list

type CreateAccountError =
    | FetchError of FetchError
    | AccountAlreadyExist of Money
    | SeveralAccountsExists of Money list

type PostResponse =
    {
        ok: int
    }

type DeleteResponse =
    {
        ok: int
    }

type AddMoneyError =
    | FetchError of FetchError
    | FilterMoneysError of FilterMoneysError

let mutable headers = [ ]

let filterMoneys (id: string) (moneys: Money list) : Result<Money, FilterMoneysError> =
    let moneyFiltered = moneys |> List.filter (fun x -> x.discordID = id)
    
    if moneyFiltered.Length = 0 then MoneyNotFound |> Result.Error
    elif moneyFiltered.Length > 1 then MultipleMoney moneyFiltered |> Result.Error
    else moneyFiltered.[0] |> Result.Ok

let fetchMoneys() : JS.Promise<Result<Money list, FetchError>> =
    promise {
        return! Fetch.tryGet("https://myserver.titaye.dev/pichou_money", headers = headers)
    }

let createAccount (id: string) (allMoneys: Money list) : JS.Promise<Result<unit, CreateAccountError>> =
    let accountAlreadyExist = allMoneys |> filterMoneys id

    match accountAlreadyExist with
    | Ok x -> AccountAlreadyExist x |> Result.Error |> Promise.lift
    | Error MoneyNotFound ->
            promise {
                let data =
                    {
                        _id = id
                        discordID = id
                        money = 0
                    }

                let! response = Fetch.tryPost("https://myserver.titaye.dev/pichou_money", data, headers = headers)

                match response with
                | Ok x -> return x |> Result.Ok
                | Error x -> return CreateAccountError.FetchError x |> Result.Error
            }
    | Error (MultipleMoney x) -> SeveralAccountsExists x |> Result.Error |> Promise.lift

let deleteAccount (id: string) : JS.Promise<Result<DeleteResponse, FetchError>> =
    promise {
        return! Fetch.tryDelete((sprintf "https://myserver.titaye.dev/pichou_money/%s" id), headers = headers)
    }

let addMoneyInAccount amount id discordID (allMoneys: Money List): JS.Promise<Result<Money, AddMoneyError>> =
    promise {
        let account = allMoneys |> filterMoneys discordID

        match account with
        | Ok x ->
            let newAccount = {
                    _id = x._id
                    discordID = x._id
                    money = x.money + amount
                }

            let url = sprintf "https://myserver.titaye.dev/pichou_money/%s" id
            let! putResult = Fetch.tryPut(url, newAccount, headers = headers)

            match putResult with
            | Ok x -> return x |> Result.Ok
            | Error x -> return FetchError x |> Result.Error
        | Error x -> return FilterMoneysError x |> Result.Error
    }

let catchFetchError (discord: IDiscord) (message: IDiscordMsg) (myError: FetchError) : unit =
    let embed = discord.CreateEmbed()
    embed.setColor("#ff0000")

    match myError with
    | FetchError.DecodingFailed x -> embed.addField("Oups !", (sprintf "Il y a eu une erreur de décodage: %A" x))
    | FetchError.FetchFailed x -> embed.addField("Oups !", (sprintf "La requête à échouée: %A" x))
    | FetchError.NetworkError x -> embed.addField("Oups !", (sprintf "Network error: %A" x))
    | FetchError.PreparingRequestFailed x -> embed.addField("Oups !", (sprintf "Erreur de préparation de la requête: %A" x))

    message.channel.send(embed) |> ignore


let initHeaders token =
    headers <- [ Fetch.Types.HttpRequestHeaders.Authorization token ]

let sendSalaryAtAllUsers amount =
    promise {
        let! allAccounts = fetchMoneys()

        match allAccounts with
        | Ok allAccounts ->
            allAccounts |> List.iter (fun x ->
                promise {
                    let! addMoneyResult = allAccounts |> addMoneyInAccount amount x._id x.discordID

                    match addMoneyResult with
                    | Ok _x -> ignore |> ignore
                    | Error (FetchError x) -> printfn "%A" x
                    | Error (FilterMoneysError x) -> printfn "%A" x
                } |> Promise.start
            )
        | Error x -> printfn "Salary error: %A" x
    } |> Promise.start

let showMoneyCommand (discord: IDiscord) (message: IDiscordMsg) =
    promise {
        let! moneyList = fetchMoneys()
        let embed = discord.CreateEmbed()

        match moneyList with
        | Ok x ->
            match x |> filterMoneys message.author.id with
            | Ok x ->
                embed.setColor("#00ffdf")
                embed.addField("Wow !", (sprintf "%d dochis" x.money))
                message.channel.send embed |> ignore
            | Error MoneyNotFound ->
                embed.setColor("#ff0000")
                embed.addField("Oups !", "Tu n'as pas de compte !\nPour en créer un utilise la commande `P! create-bank-account`")
                message.channel.send embed |> ignore
            | Error (MultipleMoney _) ->
                embed.setColor("#ff0000")
                embed.addField("Oups !", "Tu as plusieurs comptes !\nPour en un supprimer un utilise la commande `P! delete-bank-account`")
                message.channel.send embed |> ignore
        | Error x -> x |> catchFetchError discord message
    } |> Promise.start

let createAccountCommand (discord: IDiscord) (message: IDiscordMsg) =
    promise {
        let! allMoneys = fetchMoneys()
        let responseEmbed = discord.CreateEmbed()
        let mutable catchErrorUsed = false

        match allMoneys with
        | Ok x ->
            match! createAccount message.author.id x with
            | Ok _ ->
                responseEmbed.setColor("#00ffdf")
                responseEmbed.addField("Bravo !", "Ton compte est créé !\nPour voir ton argent, utilise la commande `P! money`")
            | Error (CreateAccountError.FetchError x) ->
                match x with
                | DecodingFailed x ->
                    printfn "Fetch Error, Decoding Failed: %A" x
                    responseEmbed.setColor("#ff0000")
                    responseEmbed.addField("Oups !", "Il y a eu une erreur: Fetch Error, Decoding Failed")
                | FetchFailed x ->
                    printfn "Fetch Error, Fetch Failed: %A" x
                    responseEmbed.setColor("#ff0000")
                    responseEmbed.addField("Oups !", "Il y a eu une erreur: Fetch Error, Fetch Failed")
                | NetworkError x ->
                    printfn "Fetch Error, Network Error: %A" x
                    responseEmbed.setColor("#ff0000")
                    responseEmbed.addField("Oups !", "Il y a eu une erreur: Fetch Error, Network Error")
                | PreparingRequestFailed x ->
                    printfn "Preparing Request Failed: %A" x
                    responseEmbed.setColor("#ff0000")
                    responseEmbed.addField("Oups !", "Il y a eu une erreur: Fetch Error, Preparing Request Failed")
            | Error x ->
                match x with
                | AccountAlreadyExist x ->
                    printfn "Account Already Exist: %A" x
                    responseEmbed.setColor("#ff0000")
                    responseEmbed.addField("Oups !", "Tu as déjà un compte")
                | CreateAccountError.SeveralAccountsExists x ->
                    printfn "Several Accounts Exists: %A" x
                    responseEmbed.setColor("#ff0000")
                    responseEmbed.addField("Oups !", "Tu as plusieurs comptes !\nPour en un supprimer un utilise la commande `P! delete-bank-account`")
                | CreateAccountError.FetchError x -> x |> catchFetchError discord message
        | Error x ->
            catchErrorUsed <- true
            x |> catchFetchError discord message

        if not catchErrorUsed then message.channel.send responseEmbed |> ignore
    } |> Promise.start

let deleteAccountCommand (discord: IDiscord) (bot: IClient) (message: IDiscordMsg) =
    promise {
        let! allMoneys = fetchMoneys()

        match allMoneys with
        | Ok x ->
            let moneysFiltered = x |> filterMoneys message.author.id

            match moneysFiltered with
            | Ok x ->
                    let embed = discord.CreateEmbed()

                    embed.setColor "#007fff"
                    embed.addField("Sûr ?", (sprintf "Es-tu sûr de vouloir supprimer ton compte avec %d dochis ?" x.money))
                    embed.setFooter("Pour répondre, utilise les réactions.")

                    let! embedResult = message.channel.send(embed)

                    do! embedResult.react "👍"
                    do! embedResult.react "👎"

                    let filter (reaction: IReaction) (user: IUser) =
                        (reaction.emoji.name = "👍" || reaction.emoji.name = "👎") && user.id = message.author.id

                    promise {
                        try
                            let! userResponse = embedResult.awaitReactions(filter, {| max = 1; time = 30000 |})

                            if (userResponse.first().emoji.name) = "👍" then
                                let! fetchResponse = deleteAccount x._id

                                match fetchResponse with
                                | Ok _ ->
                                    //? send response in discord
                                    let embed = discord.CreateEmbed()
                                    embed.setColor("#00ffdf")
                                    embed.addField("Voilà !", (sprintf "Ton compte est supprimé avec %d dochis" x.money))
                                    message.channel.send embed |> ignore

                                | Error x -> x |> catchFetchError discord message
                            elif (userResponse.first().emoji.name) = "👎" then
                                //? send cancel in discord
                                let embed = discord.CreateEmbed()
                                embed.setColor("#00ffdf")
                                embed.addField("Annuler !", "La procédure a été annulée.")
                                message.channel.send embed |> ignore
                        with
                        | _error ->
                            let embed = discord.CreateEmbed()
                            embed.setColor("#ff0000")
                            embed.addField("Tu n'es pas très concentré !", "Le temps est écoulé et pour des raisons de sécurité la procédure est annulée.")
                            message.channel.send embed |> ignore
                    } |> Promise.start
            | Error MoneyNotFound -> printfn "MoneyNotFound"
            | Error (MultipleMoney x) ->
                let embed = discord.CreateEmbed()

                //? add accounts in embed
                x |> List.iteri (fun i x ->
                    let title = sprintf "Choix %d" (i + 1)
                    embed.addField(title, (sprintf "Ce compte contient %d dochis" x.money))
                )

                embed.setColor "#007fff"
                embed.setTitle "Quel compte veux-tu conserver ?"
                embed.setFooter "Pour choisir, écris juste le numéro du choix.\nPour annuler, écris \"cancel\""

                do message.channel.send embed |> ignore

                promise {
                    try
                        let! embedResponse = message.channel.awaitMessages((fun thisMessage -> thisMessage.author.id = message.author.id), {| max = 1; time = 30000 |})
                        let mutable amountMoneyWillDelete = 0
                        let mutable accountToDeleteList = []

                        let responseParsed = TryParse (embedResponse.first().content)

                        match responseParsed with
                        | Some intResponse ->
                            if intResponse > x.Length || intResponse = 0 then
                                //? send error in discord
                                let embed = discord.CreateEmbed()
                                embed.setColor("#ff0000")
                                embed.addField("Oups !", "Ce chiffre n'est pas dans la liste des proposition. La procédure a été annulée.")
                                message.channel.send embed |> ignore
                            else
                                let mutable thereIsError = None

                                //? store amount will be deleted
                                x |> List.iteri (fun i x ->
                                    if not (intResponse = i + 1) then
                                        amountMoneyWillDelete <- amountMoneyWillDelete + x.money
                                        accountToDeleteList <- List.append accountToDeleteList [ x._id ]
                                )

                                //? delete accounts in data base
                                for x in accountToDeleteList do
                                    let! deleteAccountResponse = deleteAccount x

                                    match deleteAccountResponse with
                                    | Error x -> thereIsError <- Some x
                                    | Ok x -> x |> ignore

                                match thereIsError with
                                | None ->
                                    //? send response in discord
                                    let embed = discord.CreateEmbed()
                                    embed.setColor("#00ffdf")
                                    embed.addField("Voilà !", (sprintf "Tout tes compte on été supprimés avec %d dochis" amountMoneyWillDelete))
                                    message.channel.send embed |> ignore
                                | Some x -> x |> catchFetchError discord message
                        | None ->
                                if embedResponse.first().content.ToLower() = "cancel" then
                                    //? send cancel in discord
                                    let embed = discord.CreateEmbed()
                                    embed.setColor("#00ffdf")
                                    embed.addField("Annuler !", "La procédure a été annulée.")
                                    message.channel.send embed |> ignore
                                else
                                    //? send error in discord
                                    let embed = discord.CreateEmbed()
                                    embed.setColor("#ff0000")
                                    embed.addField("Oups !", "Ce n'est pas une des proposition. La procédure a été annulée.")
                                    message.channel.send embed |> ignore
                    with
                    | _error ->
                        let embed = discord.CreateEmbed()
                        embed.setColor("#ff0000")
                        embed.addField("Tu n'es pas très concentré !", "Le temps est écoulé et pour des raisons de sécurité la procédure est annulée.")
                        message.channel.send embed |> ignore
                } |> Promise.start

        | Error x -> x |> catchFetchError discord message
    } |> Promise.start