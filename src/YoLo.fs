[<AutoOpen>]
module YoLo

open Fable.Core
open Fable.Core.JsInterop
open System.Text.RegularExpressions

type Action =
    | Empty
    | Name of string
    | NameWithArgs of string * string

type String =
    static member inline Replace (oldValue: string) (newValue: string) (str: string) : string =
        str.Replace (oldValue, newValue)
    static member inline Split separator (str: string) : string [] =
        str.Split separator
    static member inline Remove (startIndex: int) (str: string) : string =
        str.Remove startIndex

type Array =
    static member inline RemoveLast array : 'T [] =
        array
        |> Array.indexed
        |> Array.filter (fun (index, _x) ->
            index <> (array.Length - 1)
        )
        |> Array.map (fun (_idx, x) -> x)

let isEven x = (x % 2) = 0
let isOdd x = isEven x = false

[<Emit("$0.normalize(\"NFD\").replace(/[\\u0300-\\u036f]/g, \"\")")>]
let normalize (_string: string) : string = jsNative

let Capitalize (string: string) : string =
    sprintf "%s%s" (string.Chars(0).ToString().ToUpper()) (string.Substring 1)

let ToCode string : string =
    sprintf "`%A`" string

let ToLongCode string : string =
    sprintf "```%A```" string

let TryParse (string: string) =
    match System.Int32.TryParse string with
    | true, value -> Some value
    | false, _ -> None

let splitOnce (c : char) (s : string) =
    match s.Split([| c |], 2) with
    | [| a ; b |] -> (a, Some b)
    | _ -> (s, None)

let yearDuration = 365.2425
let monthDuration = yearDuration / 12.0

let (|Pichou|_|) (str: string) =
    let rg = JS.Constructors.RegExp.Create(@"^P!(chou)?(\s+(.+?))?\s*$", "gi")

    rg.Matches(str.ToLower().TrimEnd())
    |> Seq.cast<Match>
    |> Seq.tryHead
    |> Option.map ( fun r ->
        let command = r?(3) |> Option.ofObj
        match command with
        | Some cmd ->
            let head, tail = cmd |> splitOnce ' '
            let h = head |> normalize

            match tail with
            | Some t -> Action.NameWithArgs (h, t)
            | None -> Action.Name h
        | None -> Action.Empty
    )

let (|EqualsCi|_|) (str: string) arg =
    if System.String.Compare(str |> normalize, arg, System.StringComparison.OrdinalIgnoreCase) = 0
    then Some()
    else None

let (|ContainsCi|_|) (strToFind: string) (str: string) =
    if (str |> normalize).IndexOf(strToFind) >= 0
    then Some()
    else None