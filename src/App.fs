module App

open System

open Fable.Core
open Fable.Core.JsInterop

open Dotenv

open Types
open Types.Discord

open Pokemon
open Rocket
open Money


[<ImportDefault("discord.js")>]
let Discord: IDiscord = jsNative
let bot = Discord.Client()

[<ImportDefault("node-schedule")>]
let schedule: ISchedule = jsNative

[<Emit("undefined")>]
let IUserUndefined: IUser = jsNative

[<Emit("const fetch = require(\"node-fetch\"); if (!globalThis.fetch) globalThis.fetch = fetch;")>]
let fetchFun() = jsNative
fetchFun()

[<Emit("require(\"../package.json\")")>]
let packageJsonFile: NodeJs.IPackageJson = jsNative

//? schedule

let versionMessage = sprintf "Version %s %s" packageJsonFile.version packageJsonFile.version_name
let mutable scheduleCount = 0

let startScheduleStatus() =
    schedule.scheduleJob("*/5 * * * * *", (fun _ ->
        if scheduleCount = 0 then
            bot.user.setActivity("P!chou ou P!", {| ``type`` = "LISTENING" |})
            scheduleCount <- 1
        elif scheduleCount = 1 then
            bot.user.setActivity("cache-cache avec Noomi", {| ``type`` = "PLAYING" |})
            scheduleCount <- 2
        elif scheduleCount = 2 then
            bot.user.setActivity(versionMessage, {| ``type`` = "WATCHING" |})
            scheduleCount <- 0
    ))

let startScheduleSalary() =
    schedule.scheduleJob("0 18 * * *", (fun _ ->
        sendSalaryAtAllUsers 100
    ))

bot.on("ready", (fun _ ->
    printfn "I'm ready : %s" bot.user.tag

    bot.user.setActivity("P!chou ou P!", {| ``type`` = "LISTENING" |})

    Money.initHeaders (getEnv "MY_API_TOKEN")
    SoundBox.createEmbedsList Discord (getEnv "SOUNDBOX_SOURCE")

    if ((getEnv "IN_DEV") = "false") then
        startScheduleStatus()
    else
        bot.user.setActivity("Mise à jour en cours...", {| ``type`` = "WATCHING" |})
    startScheduleSalary()
))


//? Help Function
let help (message: IDiscordMsg) =
    Discord.CreateEmbed()
    |> Embed.setColor "007fff"
    |> Embed.setAuthor
        "Pichou commands"
        (bot.user.displayAvatarURL({| dynamic = true |}))
        (getEnv "INVITATION_LINK")

    |> Embed.addField "Pichou fait une recherche google" "`P! Google ...`" true
    |> Embed.addField "Pichou te montre ton avatar ou celui de quelqu'un d'autre si il est mentionné." "`P! Avatar ...`" true
    |> Embed.addField "Pichou montre la latence." "`P! Ping` | `P!ng`" true
    |> Embed.addField "Pichou fais le pokédex !" "`P! Pokémon ...`" true
    |> Embed.addField "Pichou le prochain vol spatial ! 🚀" "`P! Rocket ...`" true
    |> Embed.addField "Pichou te montre ton ARGENT ! 💸" "`P! money`" true
    |> Embed.addField "Pichou te créer un compte bancaire ! 💸" "`P! create-bank-account`" true
    |> Embed.addField "Pichou te DÉTRUIT ton compte bancaire ! 🚨" "`P! delete-bank-account`" true
    |> Embed.addField "Pichou rejoin ton salon vocal et joue le song demandé ! 🔊" "`P! soundbox ...`" true
    |> Embed.addField "Pichou te montre le cumule du retard de la sncf de la veille ! 🚄⏱️" "`P! sncfislate | P! sncfretard`" true

    |> Embed.setThumbnail (bot.user.displayAvatarURL({| dynamic = true |}))

    |> message.channel.send
    |> ignore


//? Ping Function
let ping (message: IDiscordMsg) =
    let timestamp = DateTimeOffset(DateTime.Now).ToUnixTimeMilliseconds()
    message.channel.send(sprintf ":ping_pong: | %i ms" (timestamp - message.createdTimestamp)) |> ignore


//? avatar
let avatar (message: IDiscordMsg) =
    let senderUrl = message.author.displayAvatarURL({| dynamic = true |}) + "?size=1024"
    let mention = message.mentions.getFirst()
    let mutable response = ""

    if message.mentions.everyone then
        response <- "Tu ne peut pas récupérer l'avatar de tout le monde"
    elif mention = IUserUndefined then
        response <- senderUrl
    else
        response <- mention.displayAvatarURL({| dynamic = true |}) + "?size=1024"

    message.channel.send(response) |> ignore


//? Google function
let google (message: IDiscordMsg) (args: string) =
    let argsConverted = args.Replace(" ", "%20")
    message.reply(sprintf "https://www.google.com/search?q=%s" argsConverted)


//? RIP
let rip (message: IDiscordMsg) =
    let allGif = [
        "https://tenor.com/view/good-bye-harry-potter-daniel-radcliffe-waving-train-gif-5021009"
        "https://media.giphy.com/media/KEkOcB5DJ4E4bhou9T/giphy.gif"
        "https://tenor.com/view/spongebob-bury-gif-4949736"
        "https://tenor.com/view/rip-coffin-black-ghana-celebrating-gif-16743302"
    ]

    let random = Random()

    if (message.content.ToLower().Contains "adieu") then
        message.channel.send(allGif.[random.Next(allGif.Length)]) |> ignore


//? Command not found
let commandNotFound (message: IDiscordMsg) =
    let embed = Discord.CreateEmbed()
    embed.setColor("#ff0000")
    embed.addField("Oups !", "Cette commande n'éxiste pas.", true)

    message.channel.send embed |> ignore


bot.on("message", (fun message ->
    match message.content with
        | EqualsCi "p!ng" -> ping message //? Ping
        | ContainsCi "adieu" -> rip message //? RIP
        | Pichou x ->
            match x with
                | Action.Name "help" -> help message //? help
                | Action.Name "ping" -> ping message //? Ping
                | Action.NameWithArgs ("avatar", _)
                | Action.Name "avatar" -> avatar message //? Avatar
                | Action.Name "rocket" -> rocketFunction Discord message //? Rocket
                | Action.NameWithArgs ("google", args) -> google message args
                | Action.NameWithArgs ("pokemon", args) -> pokemon Discord message args //? Pokémon
                | Action.Name "money" -> showMoneyCommand Discord message
                | Action.Name "create-bank-account" -> createAccountCommand Discord message
                | Action.Name "delete-bank-account" -> deleteAccountCommand Discord bot message
                | Action.Name "soundbox" -> SoundBox.postChoices Discord message None
                | Action.NameWithArgs ("soundbox", args) -> SoundBox.postChoices Discord message (Some args)
                | Action.Name "sncfislate"
                | Action.Name "sncfretard" -> Sncf.postSncfLate Discord bot message
                | Action.NameWithArgs _
                | Action.Name _ -> commandNotFound message //? Command not found
                | Action.Empty ->
                    let allAnswers = ["Quoi ? 😑"; "Oui ? 😁"; "Je suis là, que veux tu faire ? 😉"; "Oui-stiti ! 😅"; "Salut copain, tu veut un truc ? 😃"]
                    let random = Random()
                    message.channel.send(allAnswers.[random.Next(allAnswers.Length)]) |> ignore
        | _ -> ()
))

bot.login(getEnv "PICHOU_SECRET_KEY")