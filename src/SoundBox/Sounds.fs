module Sounds

open Fable.Core
open Fable.Core.JsInterop

type ButtonPosition =
    | Left
    | Middle
    | Right


type SoundObject =
    { path: string
      name: string
      mutable isPlaying: bool
      imagePath: string
      position: ButtonPosition }


let sncf =
    { path = "/Sounds/sncf.mp3"
      name = "sncf"
      isPlaying = false
      imagePath = "/Images/logo-sncf.png"
      position = Left }

let ah =
    { path = "/Sounds/ah.mp3"
      name = "ah"
      isPlaying = false
      imagePath = "/Images/ah.png"
      position = Middle }

let toca =
    { path = "/Sounds/tocata.mp3"
      name = "toca"
      isPlaying = false
      imagePath = "/Images/minions.png"
      position = Right }

let tac =
    { path = "/Sounds/tac.mp3"
      name = "tac"
      isPlaying = false
      imagePath = "/Images/clav.png"
      position = Left }

let bull =
    { path = "/Sounds/bull.mp3"
      name = "bull"
      isPlaying = false
      imagePath = "/Images/bulle.png"
      position = Middle }

let sonnette =
    { path = "/Sounds/sonette.mp3"
      name = "sonnette"
      isPlaying = false
      imagePath = "/Images/sonnette.png"
      position = Right }

let tombo =
    { path = "/Sounds/tombo.mp3"
      name = "tombo"
      isPlaying = false
      imagePath = "/Images/tambour.png"
      position = Left }

let bol =
    { path = "/Sounds/bol.mp3"
      name = "bol"
      isPlaying = false
      imagePath = "/Images/bol.png"
      position = Middle }

let marche =
    { path = "/Sounds/marche.mp3"
      name = "marche"
      isPlaying = false
      imagePath = "/Images/marche.png"
      position = Right }

let av =
    { path = "/Sounds/avengers.mp3"
      name = "av"
      isPlaying = false
      imagePath = "/Images/avengers.png"
      position = Left }

let non =
    { path = "/Sounds/gotaga.mp3"
      name = "non"
      isPlaying = false
      imagePath = "/Images/non.png"
      position = Middle }

let coucou =
    { path = "/Sounds/coucou.mp3"
      name = "coucou"
      isPlaying = false
      imagePath = "/Images/coucou.png"
      position = Right }

let temp =
    { path = "/Sounds/tempate.mp3"
      name = "temp"
      isPlaying = false
      imagePath = "/Images/temp.png"
      position = Left }

let rob =
    { path = "/Sounds/roblox.mp3"
      name = "rob"
      isPlaying = false
      imagePath = "/Images/roblox.png"
      position = Middle }

let prout =
    { path = "/Sounds/prout.mp3"
      name = "prout"
      isPlaying = false
      imagePath = "/Images/prout.png"
      position = Right }

let gifi =
    { path = "/Sounds/gifi.mp3"
      name = "gifi"
      isPlaying = false
      imagePath = "/Images/gifi.png"
      position = Left }

let sat =
    { path = "/Sounds/saturer.mp3"
      name = "sat"
      isPlaying = false
      imagePath = "/Images/sat.png"
      position = Middle }

let swing =
    { path = "/Sounds/swing.mp3"
      name = "swing"
      isPlaying = false
      imagePath = "/Images/elec.png"
      position = Right }

let death =
    { path = "/Sounds/death.mp3"
      name = "death"
      isPlaying = false
      imagePath = "/Images/death.png"
      position = Left }

let popo =
    { path = "/Sounds/popo.mp3"
      name = "popo"
      isPlaying = false
      imagePath = "/Images/médoc-liquide.png"
      position = Middle }

let shot =
    { path = "/Sounds/headshot.mp3"
      name = "shot"
      isPlaying = false
      imagePath = "/Images/headshot.png"
      position = Right }

let verre =
    { path = "/Sounds/verre.mp3"
      name = "verre"
      isPlaying = false
      imagePath = "/Images/verre.png"
      position = Left }

let goute =
    { path = "/Sounds/eau.mp3"
      name = "goute"
      isPlaying = false
      imagePath = "/Images/goute.png"
      position = Middle }

let coq =
    { path = "/Sounds/coq.mp3"
      name = "coq"
      isPlaying = false
      imagePath = "/Images/coq.png"
      position = Right }

let poule =
    { path = "/Sounds/poule.mp3"
      name = "poule"
      isPlaying = false
      imagePath = "/Images/poulet.png"
      position = Left }

let fox =
    { path = "/Sounds/twentyth_century_fox.mp3"
      name = "fox"
      isPlaying = false
      imagePath = "/Images/20th_century_fox.png"
      position = Middle }

let engine =
    { path = "/Sounds/engine.mp3"
      name = "engine"
      isPlaying = false
      imagePath = "/Images/ferrari.png"
      position = Right }

let universal =
    { path = "/Sounds/universal_studios.mp3"
      name = "universal"
      isPlaying = false
      imagePath = "/Images/universal.png"
      position = Left }

let mario =
    { path = "/Sounds/mario.mp3"
      name = "mario"
      isPlaying = false
      imagePath = "/Images/level.png"
      position = Middle }

let piece =
    { path = "/Sounds/piece.mp3"
      name = "piece"
      isPlaying = false
      imagePath = "/Images/piece.png"
      position = Right }

let allSounds =
    [ sncf; ah; toca
      tac; bull; sonnette
      tombo; bol; marche
      av; non; coucou
      temp; rob; prout
      gifi; sat; swing
      death; popo; shot
      verre; goute; coq
      poule; fox; engine
      universal; mario; piece ]