namespace Types

module rec Discord =

    open Fable.Core
    open Fable.Core.JsInterop

    type ISchedule =
        abstract scheduleJob: string * cb: (unit -> unit) -> unit


    type IUser =
        abstract tag: string
        abstract id: string
        abstract setStatus: status: string -> unit
        abstract setActivity: name: string * options: obj -> unit
        abstract displayAvatarURL: ?options: obj -> string

    type IEmbed =
        abstract setTitle: title: string -> unit
        abstract setDescription: description: string -> unit
        abstract setColor: color: string -> unit
        abstract setAuthor: name: string * avatarURL: string * redirectURL: string -> unit
        abstract addField: name: string * value: string * ?isInline: bool -> unit
        abstract setThumbnail: url: string -> unit
        abstract setFooter: text: string * ?iconURL: string -> unit

    module Embed =
        let setTitle title (embed: IEmbed) =
            embed.setTitle title
            embed

        let setDescription description (embed: IEmbed) =
            embed.setDescription description
            embed

        let setColor color (embed: IEmbed) =
            embed.setColor color
            embed

        let setAuthor name avatarURL redirectURL (embed: IEmbed) =
            embed.setAuthor(name, avatarURL, redirectURL)
            embed

        let addField name value isInline (embed: IEmbed) =
            embed.addField(name, value, isInline)
            embed

        let setThumbnail url (embed: IEmbed) =
            embed.setThumbnail url
            embed

        let setFooter text iconUrlOpt (embed: IEmbed) =
            match iconUrlOpt with
            | Some iconUrl -> embed.setFooter(text, iconUrl)
            | None -> embed.setFooter text
            embed

    type IEmbedField =
        {
            name: string
            value: string
            ``inline``: bool
        }

    type IEmbedObj =
        {
            title: string
            description: string
            color: int
            fields: IEmbedField list option
        }

    type IMentions =
        abstract everyone: bool
        abstract users: JS.Object
        [<Emit("$0.users.array()[0]")>]
        abstract getFirst: unit -> IUser

    type IEmoji =
        abstract name: string

    type IReaction =
        abstract emoji: IEmoji

    type IReactionCollector =
        abstract stop: unit -> unit
        abstract on: event: string * (IReaction -> unit) -> unit

    type IDiscordCollection<'a> =
        abstract first: unit -> 'a
        abstract size: int

    type File =
        {
            attachment: string
            name: string
        }

    type SendFileOption =
        {
            files: File list
        }

    type IChannel =
        abstract send: string -> JS.Promise<IDiscordMsg>
        abstract send: IEmbed -> JS.Promise<IDiscordMsg>
        abstract send: SendFileOption -> JS.Promise<IDiscordMsg>
        abstract awaitMessages: filter: (IDiscordMsg -> bool) * ?options: obj -> JS.Promise<IDiscordCollection<IDiscordMsg>>

    type PlaySoundResult =
        abstract on : string * (string -> unit) -> unit

    type VoiceConnection =
        abstract play: string -> PlaySoundResult
        abstract disconnect: unit -> unit

    type VoiceChannel =
        abstract join: unit -> JS.Promise<VoiceConnection>

    type VoiceState =
        abstract channel: VoiceChannel

    type GuildMember =
        abstract voice: VoiceState

    type IDiscordMsg =
        abstract author: IUser
        abstract content: string
        abstract channel: IChannel
        abstract ``member``: GuildMember
        abstract createdTimestamp: int64
        abstract mentions: IMentions
        abstract reply: content: string * ?options: obj -> unit
        abstract react: string -> JS.Promise<unit>
        abstract createReactionCollector: filter: (IReaction -> IUser -> bool) * ?options: obj -> IReactionCollector
        abstract awaitReactions: filter: (IReaction -> IUser -> bool) * ?options: obj -> JS.Promise<IDiscordCollection<IReaction>>
        abstract delete: unit -> unit

    type IClient =
        abstract on: event: string * func: (IDiscordMsg -> unit) -> unit
        abstract user: IUser
        abstract login: token: string -> unit

    type IDiscord =
        [<Emit("new $0.Client()")>]
        abstract Client: unit -> IClient
        [<Emit("new $0.MessageEmbed()")>]
        abstract CreateEmbed: unit -> IEmbed
        [<Emit("new $0.MessageEmbed($1)")>]
        abstract CreateEmbed: data: obj -> IEmbed