module Pokemon

open Fable.Core
open Types.Discord
open Types.PokedexTypes
open Thoth.Fetch
open Thoth.Json


let fetchPokemon (args: string) : JS.Promise<Result<IPokeAPI, FetchError>> =
    promise {
        let url = sprintf "https://pokeapi.co/api/v2/pokemon/%s" args
        return! Fetch.tryGet(url, caseStrategy = CamelCase)
    }

let fetchDescriptionPokemon (url: string) : JS.Promise<IPokeAPIDescription> =
    promise {
        return! Fetch.get(url, caseStrategy = SnakeCase)
    }

let predicateForGetDescription (e: IPokeDescription) = e.language.name = "fr" && e.version.name = "omega-ruby"

let getPokemonImage (pokemonJson: IPokeAPI) =
    match pokemonJson.sprites.other.``official-artwork``.front_default with
        | Some x -> x
        | None -> pokemonJson.sprites.front_default

//? Pokémon Function
let pokemon (discord: IDiscord) (message: IDiscordMsg) (args: string) =
    promise {
        let! pokemonJson = fetchPokemon args

        match pokemonJson with
        | Ok pokemonJson ->
            let! descriptionJson = fetchDescriptionPokemon pokemonJson.species.url

            //? get the description in french for the omega ruby version
            let description = (descriptionJson.flavor_text_entries |> List.filter predicateForGetDescription).[0].flavor_text.Replace("\n", " ")

            let embed = discord.CreateEmbed()
            embed.setColor("007fff")
            embed.setThumbnail(getPokemonImage pokemonJson)
            embed.setTitle(Capitalize pokemonJson.name)

            embed.addField(":dividers: Type", sprintf "%A" pokemonJson.types.[0].``type``.name, true)
            embed.addField(":scales: Poids", sprintf "%A" pokemonJson.weight, true)
            embed.addField(":triangular_ruler: Taille", sprintf "%A" pokemonJson.height, true)

            embed.addField("Pv", pokemonJson.stats.[0].base_stat |> ToCode, true)
            embed.addField("Attaque", pokemonJson.stats.[1].base_stat |> ToCode, true)
            embed.addField("Défense", pokemonJson.stats.[2].base_stat |> ToCode, true)
            embed.addField("Attaque Spécial", pokemonJson.stats.[3].base_stat |> ToCode, true)
            embed.addField("Défense Spécial", pokemonJson.stats.[4].base_stat |> ToCode, true)
            embed.addField("Vitesse", pokemonJson.stats.[5].base_stat |> ToCode, true)

            embed.addField("Description", description |> ToLongCode)

            message.channel.send embed |> ignore
        | Error error ->
            match error with
            | FetchFailed x ->
                if x.Status = 404 then
                    let embed = discord.CreateEmbed()
                    embed.setColor("#ff0000")
                    embed.addField("Oups !", "Ce pokémon n'existe pas, peut-être que ce n'est pas la bonne écriture")

                    message.channel.send(embed) |> ignore
            | NetworkError x -> printfn "%A" x.Message
            | PreparingRequestFailed x -> printfn "%A" x.Message
            | DecodingFailed x -> printfn "%A" x
    } |> Promise.start