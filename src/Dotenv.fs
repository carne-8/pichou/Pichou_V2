[<AutoOpen>]
module Dotenv

open System
open Fable.Core
open Fable.Core.JsInterop

[<Emit("require(\"dotenv\").config(); process.env[$0] ? process.env[$0] : \"\"")>]
let getEnv (_key: string) : string = jsNative