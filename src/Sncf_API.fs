module Sncf

open System
open Fable.Core
open Thoth.Fetch

open Types.Discord
open Types.SncfAPI


let mutable headers = [ ]

let initHeaders token =
    headers <- [ Fetch.Types.HttpRequestHeaders.Authorization token ]


let getSinceString() =
    let now = DateTime.Now
    (now.AddDays -1.0).ToString("yyyy-MM-ddTHH:mm:ss")

let getUntilString() =
    DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss")

let stringToTimeString (string: string) =
    let hours = string.[0..1]
    let minutes = string.[2..3]
    let seconds = string.[4..5]

    sprintf "%s:%s:%s" hours minutes seconds

let timeSpanToReadable (timeSpan: TimeSpan) : string =
    let days = timeSpan.Days
    let hours = timeSpan.Hours
    let minutes = timeSpan.Minutes

    let month = int (float days / YoLo.monthDuration)
    printfn "month: %i" month
    let restDaysFromMonth = days - month * 30
    printfn "restDaysFromMonth: %i" restDaysFromMonth
    let week = restDaysFromMonth / 7
    printfn "week: %i" week
    let restDays = restDaysFromMonth - week * 7
    printfn "restDays: %i" restDays

    if month > 0 then
        if week > 0 then
            if restDays > 0 then
                if hours > 0 then
                    if minutes > 0 then
                        sprintf "%i mois, %i semaines, %i jours, %i heures et %i minutes" month week restDays hours minutes
                    else
                        sprintf "%i mois, %i semaines, %i jours et %i heures" month week restDays hours
                else
                    if minutes > 0 then
                        sprintf "%i mois, %i semaines, %i jours et %i minutes" month week restDays minutes
                    else
                        sprintf "%i mois, %i semaines et %i jours" month week restDays
            else
                if hours > 0 then
                    if minutes > 0 then
                        sprintf "%i mois, %i semaines, %i heures et %i minutes" month week hours minutes
                    else
                        sprintf "%i mois, %i semaines et %i heures" month week hours
                else
                    if minutes > 0 then
                        sprintf "%i mois, %i semaines et %i minutes" month week minutes
                    else
                        sprintf "%i mois et %i semaines" month week
        else
            if restDays > 0 then
                if hours > 0 then
                    if minutes > 0 then
                        sprintf "%i mois, %i jours, %i heures et %i minutes" month restDays hours minutes
                    else
                        sprintf "%i mois, %i jours et %i heures" month restDays hours
                else
                    if minutes > 0 then
                        sprintf "%i mois, %i jours et %i minutes" month restDays minutes
                    else
                        sprintf "%i mois et %i jours" month restDays
            else
                if hours > 0 then
                    if minutes > 0 then
                        sprintf "%i mois, %i heures et %i minutes" month hours minutes
                    else
                        sprintf "%i mois et %i heures" month hours
                else
                    if minutes > 0 then
                        sprintf "%i mois et %i minutes" month minutes
                    else
                        sprintf "%i mois" month
    else
        if week > 0 then
            if restDays > 0 then
                if hours > 0 then
                    if minutes > 0 then
                        sprintf "%i semaines, %i jours, %i heures et %i minutes" week restDays hours minutes
                    else
                        sprintf "%i semaines, %i jours et %i heures" week restDays hours
                else
                    if minutes > 0 then
                        sprintf "%i semaines, %i jours et %i minutes" week restDays minutes
                    else
                        sprintf "%i semaines et %i jours" week restDays
            else
                if hours > 0 then
                    if minutes > 0 then
                        sprintf "%i semaines, %i heures et %i minutes" week hours minutes
                    else
                        sprintf "%i semaines et %i heures" week hours
                else
                    if minutes > 0 then
                        sprintf "%i semaines et %i minutes" week minutes
                    else
                        sprintf "%i semaines" week
        else
            if restDays > 0 then
                if hours > 0 then
                    if minutes > 0 then
                        sprintf "%i jours, %i heures et %i minutes" restDays hours minutes
                    else
                        sprintf "%i jours et %i heures" restDays hours
                else
                    if minutes > 0 then
                        sprintf "%i jours et %i minutes" restDays minutes
                    else
                        sprintf "%i jours" restDays
            else
                if hours > 0 then
                    if minutes > 0 then
                        sprintf "%i heures et %i minutes" hours minutes
                    else
                        sprintf "%i heures" hours
                else
                    if minutes > 0 then
                        sprintf "%i minutes" minutes
                    else
                        "Pas de retard !"



let getLatestDisruptions since until : JS.Promise<Result<ISncfAPI, FetchError>> =
    promise {
        // let url = sprintf "https://api.navitia.io/v1/coverage/sncf/disruptions/?since=%s&until=%s" since until
        let url = sprintf "https://api.navitia.io/v1/coverage/sncf/disruptions/?since=2021-02-22T00:00:00&until=2021-02-22T01:00:00"
        printfn "%s" url

        return! Fetch.tryGet(url, headers = headers)
    }



let trySncf() =
    promise {
        let! apiResponse = getLatestDisruptions (getSinceString()) (getUntilString())

        match apiResponse with
        | Ok x ->
            let mutable cumulatedDelay = TimeSpan.Zero

            x.disruptions |> List.iter (fun x ->
                x.impacted_objects.[0].impacted_stops
                |> List.iter (fun x ->
                    match x.amended_arrival_time with
                    | Some amendedArrivalString ->
                        match x.base_arrival_time with
                        | Some baseArrivalString ->
                            let baseArrivalTime = TimeSpan.Parse (stringToTimeString baseArrivalString)
                            let amendedArrivalTime = TimeSpan.Parse (stringToTimeString amendedArrivalString)

                            let delay = amendedArrivalTime.Subtract baseArrivalTime
                            cumulatedDelay <- cumulatedDelay.Add delay
                        | _ -> ignore |> ignore
                    | _ -> ignore |> ignore
                )
            )

            printfn "%A" (timeSpanToReadable(TimeSpan.FromDays(517.0)))
        | Error x ->
            printfn "error: %A" x
    } |> Promise.start